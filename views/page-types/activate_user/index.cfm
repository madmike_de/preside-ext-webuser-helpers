<cf_presideparam name="args.title"        type="string" field="page.title"        editable="true" />
<cf_presideparam name="args.main_content" type="string" field="page.main_content" editable="true" />

<cfoutput>
  <cfif len(trim(args.title)) >
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h1>#args.title#</h1>
      </div>
    </div>
  </cfif>
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      #renderContent("richeditor",args.main_content)#
      
      <cfswitch expression="#args.errorCode#">
        <cfcase value="0">Ihr Zugang wurde erfolgreich aktiviert.</cfcase>
        <cfcase value="1">Ihr Zugang ist bereits aktiviert.</cfcase>
        <cfcase value="2">Ihr Benutzer konnte nicht gefunden werden.</cfcase>
        <cfdefaultcase>Beim Aktivieren Ihres Benutzers trat ein Problem auf.</cfdefaultcase>
      </cfswitch>
      
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-12">
          #renderViewlet( event="ticker")#
        </div>
      </div>
      
      #renderViewlet( event="page-types.homepage._nextEvents")#
      
    </div>
  </div>
  <br>
</cfoutput>
