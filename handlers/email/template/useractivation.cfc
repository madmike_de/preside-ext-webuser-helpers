component {
	private struct function prepareParameters() {
		return {
			  site_url            = event.getSite().domain
			, activationlink = event.buildLink(
				  page        = "activate_user"
				, querystring = "token=" & ( arguments.userid ?: "" )
			  )
		};
	}

	private struct function getPreviewParameters() {
		return {
			activationlink = event.getSite().protocol & "://" & event.getSite().domain & "/dummy/activate_user/?token=12345"
		};
	}

}	