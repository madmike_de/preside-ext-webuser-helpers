component output=false {

	property name="websiteLoginService"   inject="websiteLoginService";
	property name="bcryptService"					inject="bcryptService";
	property name="presideObjectService"				inject="presideObjectService";
	property name="emailService"          inject="emailService";

	public void function registerUser( event, rc, prc ) output=false {

		// Check ob der User schon existiert.
		var checkUser = presideObjectService.selectData(
			objectName = "website_user"
			, filter       = "login_id = :login_id OR email_address = :email_address"
			, filterParams = { "login_id" = rc.email_address, "email_address" = rc.email_address }
			, recordCountOnly = true
		)

		// Neuen User anlegen
		if(checkUser == 0) {
			var displayName = trim(rc.vorname & ' ' & rc.nachname);
			if(isEmpty(displayName)) {
				displayName=rc.email_address;
			}
			
			var newUser = presideObjectService.insertData(
					objectName = "website_user",
					data = {
						login_id = rc.email_address,
						email_address = rc.email_address,
						password = bcryptService.hashPw( pw = rc.password),
						display_name = displayName,
						anrede = rc.anrede,
						akad = rc.akad,
						vorname = rc.vorname,
						nachname = rc.nachname,
						beruf = rc.beruf,
						firma = rc.firma,
						efn = rc.efn,
						strasse = rc.strasse,
						plz = rc.plz,
						ort = rc.ort
					}
			);

			emailService.send(
				template    = "useractivation"
				, recipientId = newUser
				, args        = { userId = newUser }
			);
			
			
			setNextEvent( uri='/login/registerDone.html' );
		} else {
			// User existiert bereits
			var persist = event.getCollectionWithoutSystemVars();
				persist.message = "USER_ALREADY_EXISTS";

			setNextEvent( url=event.buildLink( page="login" ), persistStruct=persist );
		}
		
	}
}