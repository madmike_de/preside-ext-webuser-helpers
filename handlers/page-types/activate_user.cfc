component {

	property name="userDao"               inject="presidecms:object:website_user";
	

	public function index( event, rc, prc, args={} ) {
		param name="rc.token" default="";
		
		var theUser = userDao.selectData(id=rc.token);
		
		if(theUser.recordCount == 1) { // User gefunden
			if(!theUser.active) { // Nicht aktiv -> aktivieren
				
				var updated = userDao.updateData(id=rc.token, data = {active=true} );
				
				if(updated==1) {
					args.errorCode=0;
				} else {
					args.errorCode=3;
				}
				
			} else { // User bereits aktiv -> Nix machen
				args.errorCode=1;
			}
			
		} else { // User nicht gefunden
			args.errorCode=2;
		}
	
		return renderView(
			  view          = '/page-types/activate_user/index'
			, presideObject = 'activate_user'
			, id            = event.getCurrentPageId()
			, args          = args
		);
	}

}