component {

	public void function configure( required struct config ) {
		var conf     = arguments.config;
		var settings = conf.settings ?: {};

		ArrayAppend(conf.interceptors, { class="application.extensions.preside-ext-webuser-helpers.interceptors.LoginInterceptor" , properties={} });
		
		_setupEmailTemplates( settings );
	}

	private void function _setupEmailTemplates( settings ){
		// E-Mail-Template User activation
		settings.email.templates.useractivation = {
			recipientType = "websiteUser",
			parameters    = [
				{ id="activationlink", required=true  }
			]
		};
		
	}
	
}
