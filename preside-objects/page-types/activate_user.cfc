/**
 * User Activation page
 *
 * @isSystemPageType true
 * @parentSystemPageType login
 * @feature          websiteUsers
 *
 */

component displayName="Page type: Activate User" {
}