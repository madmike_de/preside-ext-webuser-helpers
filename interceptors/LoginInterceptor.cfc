component extends="coldbox.system.Interceptor" {

// PUBLIC
	public void function configure() {}

	public void function onLoginFailure( event, interceptData ) {
		setNextEvent( url="#cgi.http_referer#?err=1" );
	}
}